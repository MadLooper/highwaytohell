/**
 * Created by KW on 10/2/2017.
 */

public enum CarType {
    TRUCK(1.23), CAR(1.0), MOTORCYCLE(0.72);
    private double priceMultiplier;

    CarType(double mul) {
        priceMultiplier = mul;
    }

    public double getPriceMultiplier() {
        return priceMultiplier;
    }
}
